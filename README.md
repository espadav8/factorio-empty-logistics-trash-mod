# factorio-empty-logistics-trash-mod

A very simple Factorio mod that adds a button to the UI when opening a chest to
dump your logistics trash into the chest.
