local function on_gui_click(event)
  local player = game.players[event.player_index]
  local gui_instance = player.gui.relative.trash

  if event.element.parent == gui_instance
    and event.element == gui_instance.empty_trash
  then
    local trash = player.get_inventory(defines.inventory.character_trash)
    local chest = player.opened.get_inventory(defines.inventory.chest)
    local completely_emptied = true

    for i=1, #trash do
      local slot = trash[i]

      if slot.valid_for_read then
        if false and chest.can_insert(slot) then
          local inserted = chest.insert(slot)
          slot.count = slot.count - inserted

          if slot.count ~= 0 then
            completely_emptied = false
          end
        else
          completely_emptied = false
        end
      end
    end

    if completely_emptied == false then
      game.print({"", "Unable to completely empty your trash"})
    end
  end
end

local function create_gui(player)
  local frame_main_anchor = {
    gui = defines.relative_gui_type.container_gui,
    position = defines.relative_gui_position.right
  }

  local frame_main = player.gui.relative.add({
    type = "frame",
    name = "trash",
    caption = "Logistics Trash",
    anchor = frame_main_anchor
  })

  local button = frame_main.add{type="button", caption="Empty trash into chest", name="empty_trash"}
end

local function delete_gui(player)
  if player.gui.relative.trash then
    player.gui.relative.trash.destroy()
  end
end

local function create_gui_all_players()
  for idx, player in pairs(game.players) do
    delete_gui(player)
    create_gui(player)
  end
end

script.on_init(create_gui_all_players)
script.on_configuration_changed(create_gui_all_players)
script.on_event({defines.events.on_player_created},
  function(event)
    local player = game.players[event.player_index]
    create_gui(player)
  end
)
script.on_event({defines.events.on_gui_click}, on_gui_click)
